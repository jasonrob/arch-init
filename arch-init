#!/usr/bin/env bash
#
# NAME: slutbox-init
# DESC: To ease re-installs and distrohopping
# WARNING: Run this script at your own risk.
# DEPENDENCIES: dialog

if [ "$(id -u)" = 0 ]; then
    echo "##################################################################"
    echo "This script MUST NOT be run as root user since it makes changes"
    echo "to the \$HOME directory of the \$USER executing this script."
    echo "The \$HOME directory of the root user is, of course, '/root'."
    echo "We don't want to mess around in there. So run this script as a"
    echo "normal user. You will be asked for a sudo password when necessary."
    echo "##################################################################"
    exit 1
fi

error() { \
    printf "ERROR:\\n%s\\n" "$1" >&2; exit 1;
}

update_sync() { \
    echo "################################################################"
    echo "## Syncing the repos and installing 'git' if not installed ##"
    echo "################################################################"
    sudo pacman --noconfirm --needed -Syu git || error "Error syncing the repos."
}

update_sync || error "Error syncing repos"

# get properties

echo "################################################################"
echo "## Setting git properties                                     ##"
echo "################################################################"

git config --global user.email "jason@caladan.ca"
git config --global user.name "Jason Robertson"

install_packages() { \
    # Let's install each package listed in the pkglist file.
    sudo pacman --needed --ask 4 -Sy - < pkglist
}

install_packages || error "Error installing packages"


echo "################################################################"
echo "## xfce keybindings                                           ##"
echo "################################################################"

install_keybind() { \
	xfconf-query --create -c xfce4-keyboard-shortcuts -t string -p '/commands/custom/<Primary><Shift>k' -v -s /usr/bin/keepassxc
	xfconf-query --create -c xfce4-keyboard-shortcuts -t string -p '/commands/custom/<Primary><Shift>b' -v -s /usr/bin/brave
	xfconf-query --create -c xfce4-keyboard-shortcuts -t string -p '/commands/custom/<Super>r' -v -s 'rofi -show run'
	xfconf-query --create -c xfce4-keyboard-shortcuts -t string -p '/commands/custom/<Super>l' -v -s 'slock'
	xfconf-query --create -c xfce4-keyboard-shortcuts -t string -p '/commands/custom/<Primary><Shift>t' -v -s 'xfce4-terminal'
	xfconf-query --create -c xfce4-keyboard-shortcuts -t string -p '/commands/custom/<Super>Return' -v -s 'xfce4-terminal'
	echo "...complete"
}

install_keybind || error "Error installing keybindings"

# start systemd services

echo "################################################################"
echo "## start systemd services                                     ##"
echo "################################################################"

enable_systemd_services() { \
	sudo systemctl enable sddm
	sudo systemctl enable ufw
	sudo systemctl enable ntpd
	echo "...complete"
}

enable_systemd_services || error "Error starting systemd services"

# apply dotfiles

echo "################################################################"
echo "## Deploy dotfiles                                            ##"
echo "################################################################"

deploy_dotfiles() { \
	if [ ! -d $HOME/dotfiles ]; then
		cd $HOME
		git clone git@gitlab.com:jasonrob/dotfiles.git 
		cd $HOME/dotfiles
		ls -1d */ | xargs -n 1 stow
		cd $HOME
	fi
	echo "...complete"
}

deploy_dotfiles || error "Error deploying dotfiles"

# installing pip modules

echo "################################################################"
echo "## Install pips                                            ##"
echo "################################################################"

install_pip() { \
	python -m pip install yamllint
	python -m pip install jsonlint
}

install_pip || error "Error installing pip modulues"
